package vrclog;
import java.util.*;
import java.io.*;

class Entry{
    private String reason;
    private String name;
    private Date   date;

    Entry(String _reason, String _name, Date _date){
	reason = _reason;
	name   = _name;
	date   = _date;
    };
    @Override 
    public  String toString(){
	return name + " | " + reason + " | " + date;
    }
}
