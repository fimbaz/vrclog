package vrclog;
import java.util.*;
import java.io.*;  
import java.text.*;
//import java.nio.*;
import java.nio.file.*;
import java.nio.charset.*;
class VRCLog{
    private static FileSystem fs = FileSystems.getDefault();
    private static Charset    cs = Charset.defaultCharset();
    private static SimpleDateFormat dateFormat = new SimpleDateFormat("M-d-y h:mma");

    private Path logPath;
    private Path reasonsPath;
    static String[] defaultReasons = {
	"Discuss a personal matter",
	"Use the microwave",
	"Use a PC",
	"Donate a book"};
    Scanner sc;
    List<String> reasons;
    
    VRCLog() throws IOException{
	sc = new Scanner(System.in);
	reasonsPath   = fs.getPath("reasons.txt");
	logPath       = fs.getPath("log.txt");
	try{
	    reasons = Files.readAllLines(reasonsPath,cs);
	} catch (IOException e)
	    {   
	    //we create reasons.txt with this text if it doesn't exist already.
	    Files.write(reasonsPath,Arrays.asList(defaultReasons),cs,
			StandardOpenOption.CREATE);
	}
	reasons = Files.readAllLines(reasonsPath,cs);

	return;
    }

    public void begin() throws IOException{	
	System.out.println("\nVeteran's Resource Center");
	System.out.println("-------------------------");
	System.out.print("Name: ");
	String name = sc.nextLine();
	String formattedDate = dateFormat.format(new Date());
	int i=1;
	for(String reason: reasons){
	    System.out.println("\t"+ i++ +". "+ reason);
	}

	System.out.print("Reason for visiting[1-"+reasons.size()+"]:");
	int reasonNumber = 0;
	try{
	    reasonNumber = sc.nextInt();
	}catch(InputMismatchException e){/*Do nothing*/}
	//throw away the input-- nextInt doesn't doesn't read in '\n'
	sc.nextLine();
	if(reasonNumber <= reasons.size() && reasonNumber > 0){
	    log(formattedDate+" "+" "+name+":\t"+ reasons.get(reasonNumber-1)+"\n");
	    System.out.println("\nThanks!  Have a nice day :)");
	}
	else{
	    System.out.println("Invalid selection!  Try again");
	}
	return;
    }
    public void log(String text) throws IOException{
	Files.write(logPath,text.getBytes(),StandardOpenOption.CREATE,StandardOpenOption.APPEND);
    }
}
